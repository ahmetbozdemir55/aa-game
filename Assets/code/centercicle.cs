﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class centercicle : MonoBehaviour
{
    
    public GameObject smallcircle;
    public Text centercircletext;
    public int smallcirclemath;
    public Text dönencemberlevel;
    public List<GameObject> SmallcircleList;
    public int levelmath;
    public GameObject CenterCircile;

    
    void Start()
    {
        levelmath = smallcirclemath;
        centercircletext.text = smallcirclemath + "";
        dönencemberlevel.text = SceneManager.GetActiveScene().name;

    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetButtonDown("Fire1"))
        {
            smallcirclecreate(); 
        }


    }
    void smallcirclecreate()
    {

        GameObject G = Instantiate(smallcircle, transform.position, transform.rotation);
        SmallcircleList.Add(G);
        if (smallcirclemath > 1)
        {

            centercircletext.text = (smallcirclemath - 1) + "";
            smallcirclemath = smallcirclemath - 1;

        }
        else
        {
            centercircletext.text = "";
            smallcirclemath = 0;
        }
       
    }
}
