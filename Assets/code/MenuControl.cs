﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuControl : MonoBehaviour
{
    public AudioSource backgroundsound;

    public void gameplay()
    {
        SceneManager.LoadScene("1");
    }
    public void gameexit()
    {
        Application.Quit();
    }
    void Update()
    {
        backgroundsound = GetComponent<AudioSource>();
    }
}
