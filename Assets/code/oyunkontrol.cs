﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class oyunkontrol : MonoBehaviour
{
    public AudioSource backgroundsound;
    GameObject turing;
    GameObject centercircle;
    public Animator animasyon;
    public GameObject levelcircle;
    public Text dönencemberlevel;
    public TextMeshProUGUI leveltext;
    public GameObject LevelButton;
    int k;
    int l;
    public oyunkontrol oyunkontrolcall;
    private centercicle centerciclego;
    GameObject centerciclegetir;
    public GameObject circle;
    public int counter = 1;
    public GameObject turingcircle;
    public GameObject Confetti;
    public centercicle centercirclecall;
    int m;
    int n;
    ParticleSystem system;
        private void OnAnimatorIK(int layerIndex)
    {
        
    }
    void Start()
    {
        centerciclegetir = GameObject.Find("centercircle");
        turing = GameObject.FindGameObjectWithTag("circleturingtag");
        centercircle = GameObject.FindGameObjectWithTag("centercircle");
        l = counter + 2;
        counter = counter + 1;
        m = counter;
        system = Confetti.GetComponent<ParticleSystem>();
        centerciclego = centerciclegetir.GetComponent<centercicle>();
    }
    private void Update()
    {
        backgroundsound = GetComponent<AudioSource>();
        if (centerciclego.smallcirclemath == 0)
        {
            leveltext.text = "  Level  " + m;
            dönencemberlevel.text = " " + m;

            //if (counter<l)
            //{
            //    counter = counter + 1;
            //}

        }
        
    }
    public void OyunBitti()
    {
        StartCoroutine(Cagrılacak());
    }
    IEnumerator Cagrılacak()
    {
        turing.GetComponent<Turing>().enabled = false;
        centercircle.GetComponent<centercicle>().enabled = false;
        animasyon.SetTrigger("GameEnd");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Menü");
    }
    public void Button()
    {
        turing.GetComponent<Turing>().enabled = true;
        centercircle.GetComponent<centercicle>().enabled = true;
        counter = counter + 1;
        n = counter - 1;
        centercirclecall.smallcirclemath = centercirclecall.levelmath + 1;
        leveltext.text = "  Level  " + counter;
        dönencemberlevel.text = " " + n;
        LevelButton.SetActive(false);
        foreach (var item in centercirclecall.SmallcircleList)
        {
            Destroy(item.gameObject);
        }

        centercirclecall.SmallcircleList.Clear();
        centercircle.SetActive(true);
        circle.SetActive(true);
        LevelButton.SetActive(false);
        turingcircle.SetActive(true);

        k = counter - 1;
        for (int i = 0; i < k; i++)
        {
            GameObject circletest = Instantiate(levelcircle, circle.transform.position, transform.rotation);
            circletest.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
            circletest.transform.SetParent(circle.transform);
        }
        


    }
}
