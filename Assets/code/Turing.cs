﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turing : MonoBehaviour
{
    public float v;
    public GameObject Confetti;
    public GameObject circle;
    public GameObject centercircle;
    public GameObject levelcircle;
    GameObject OyunKontrol;
    public GameObject turingcircle;
    public centercicle centercirclecall;
    public oyunkontrol oyunkontrolcall;
    int k;
    public GameObject LevelButton;
    public ParticleSystem confetiparticel;
    public ParticleSystem confetiparticel1;

    int nextmath;
    int counter=0;
    private void Start()
    {
        nextmath = centercirclecall.smallcirclemath;
        k = oyunkontrolcall.counter - 1;
    }
    void Update()
    {
        transform.Rotate(0, 0, v*Time.deltaTime);

    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "smalltag")
        {
            counter++;
            if (nextmath == counter)
            {
                counter = 0;

                confetiparticel.Play();
                confetiparticel1.Play();
                centercircle.SetActive(false);
                circle.SetActive(false);
                LevelButton.SetActive(true);
                turingcircle.SetActive(false);
                
            }
        }

    }
}
