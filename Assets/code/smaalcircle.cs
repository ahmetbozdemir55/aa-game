﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class smaalcircle : MonoBehaviour
{
    Rigidbody2D physics;
    public float v;
    bool circlevcontrol=false;
    GameObject OyunKontrol;
    public int nextlevel;
    
    void Start()
    {
        physics = GetComponent<Rigidbody2D>();
        OyunKontrol = GameObject.FindGameObjectWithTag("oyunkontroltag");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!circlevcontrol)
        {
            physics.MovePosition(physics.position + Vector2.up * v * Time.deltaTime);
        }
        
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "circleturingtag")
        {
            transform.SetParent(col.transform);
            circlevcontrol = true;
        }
        if(col.tag=="smalltag")
        {
            OyunKontrol.GetComponent<oyunkontrol>().OyunBitti();
        }
        if (col.tag =="levelsmalltag")
        {
            OyunKontrol.GetComponent<oyunkontrol>().OyunBitti();
        }
    }
}
